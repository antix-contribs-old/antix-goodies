��    	      d      �       �   [   �   B   =  0   �  �   �  G   ?  .   �  W   �  9       H  d   P  C   �  :   �  �   4  <   �  7   "  k   Z  D   �                                          	    A removable drive with a mounted partition was not found.
It is safe to unplug the drive(s) About to unmount:
$summarylist
Please confirm you wish to proceed. Data is being written to devices.
Please wait... Mountpoint removal failed.

A mountpoint remains present at:
$mountpointerrorlist
Check each mountpoint listed before unpluging the drive(s). Nothing has been unmounted.
Aborting as requested with no action taken. Nothing selected.
Aborting without unmounting. The following are currently mounted:
$removablelist
Choose the drive(s) to be unplugged Unmounted:
$summarylist
It is safe to unplug the drive(s) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-12-15 00:36+0200
PO-Revision-Date: 2019-01-25 15:01+0200
Last-Translator: En Kerro <inactive+ekeimaja@transifex.com>
Language-Team: Finnish (http://www.transifex.com/anticapitalista/antix-development/language/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
X-Launchpad-Export-Date: 2012-01-02 10:46+0000
 Ei löytynyt liitettyä osiota irrotettavasta asemasta.
Laite/laitteet voidaan poistaa turvallisesti Irrotetaan:
$summarylist
Vahvista haluamasi toimenpide jatkaaksesi. Kirjoitetaan sisältöä laitteille.
Ole hyvä ja odota... Liitospisteen poisto epäonnistui.

Liitospiste näyttää olevan paikassa:
$mountpointerrorlist
Tarkista jokainen liitospiste luettelosta ennen kuin irrotat laitteen/laitteet. Mitään ei ole irrotettu.
Poistutaan tekemättä muutoksia. Mitään ei ole valittuna.
Keskeytetään irrottamatta. Tällä hetkellä seuraavat laitteet ovat liitettynä:
$removablelist
Valitse irrotettava(t) laite/laitteet Irrotettu:
$summarylist
Laite/laitteet voidaan poistaa turvallisesti 